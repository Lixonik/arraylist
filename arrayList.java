import java.util.Arrays;

public class arrayList<E> {
    private static final Object[] EMPTY_DATA_ARRAY = {};

    private Object[] dataArray;
    private int size;

    public arrayList() {
        dataArray = EMPTY_DATA_ARRAY;
    }

    public arrayList(int initialSize) {
        if (initialSize > 0) {
            size = initialSize;
            dataArray = new Object[size];
        } else if (initialSize == 0) {
            dataArray = EMPTY_DATA_ARRAY;
        } else {
            throw new IllegalArgumentException("Incorrect initial size: " + initialSize);
        }
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int indexOf(Object element){
        for (int i = 0; i < size; i++) {
            if (element.equals(dataArray[i])){
                return i;
            }
        }
        return -1;
    }

    public boolean contains(Object element) {
        return indexOf(element) >= 0;
    }

    private void modifySize(int newSize) {
        size = newSize;
        dataArray = Arrays.copyOf(dataArray, size);
    }

    @SuppressWarnings("unchecked")
    public E get(int index) {
        if (index >= 0 && index < size) {
            return (E) dataArray[index];
        }
        throw new IndexOutOfBoundsException("Incorrect index: " + index);
    }

    public void clear() {
        size = 0;
        dataArray = EMPTY_DATA_ARRAY;
    }

    public void add(E element) {
        modifySize(size + 1);
        dataArray[size - 1] = element;
    }

    public void add(int index, E element) {
        if (index >= 0 && index < size + 1) {
            modifySize(size + 1);

            for (int i = size - 2; i >= index; i--) {
                dataArray[i + 1] = dataArray[i];
            }
            dataArray[index] = element;

            return;
        }
        throw new IndexOutOfBoundsException("Incorrect index: " + index);
    }

    @SuppressWarnings("unchecked")
    public E remove(int index) {
        if (index >= 0 && index < size) {
            Object element = dataArray[index];
            for (int i = index; i < size - 1; i++) {
                dataArray[i] = dataArray[i + 1];
            }
            modifySize(size - 1);

            return (E) element;
        }
        throw new IndexOutOfBoundsException("Incorrect index: " + index);
    }

    public boolean remove(Object element) {
        for (int i = 0; i < size; i++) {
            if (element.equals(dataArray[i])) {
                remove(i);
                return true;
            }
        }
        return false;
    }

    public String toString() {
        return Arrays.toString(dataArray);
    }
}
