public class Test {
    public static void main(String[] args) {
        arrayList<Integer> list = new arrayList<>();
        for (int i = 0; i < 20; i++) {
            list.add(i);
        }
        System.out.println(list.get(1));
        System.out.println(list);
        list.add(2, 1001);
        System.out.println(list);
        System.out.println(list.remove(2));
        System.out.println(list);
        System.out.println(list.remove((Integer) 16));
        System.out.println(list);
        System.out.println(list.contains(14));
        System.out.println(list.get(3));
        System.out.println(list.size());
        System.out.println(list.isEmpty());
        list.clear();
        System.out.println(list);
        System.out.println(list.size());
        System.out.println(list.isEmpty());

        arrayList<Integer> listC = new arrayList<>(5);
        System.out.println(listC);
    }
}
